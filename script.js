/*Написати реалізацію кнопки "Показати пароль".

Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, які ввів користувач, іконка змінює свій зовнішній вигляд. У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)
Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)
Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – You are welcome;
Якщо значення не збігаються - вивести під другим полем текст червоного кольору Потрібно ввести однакові значення
Після натискання на кнопку сторінка не повинна перезавантажуватись
Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.*/

let inputOne = document.querySelectorAll("input")[0];
let inputTwo = document.querySelectorAll("input")[1];
let button = document.querySelector(".btn");

let iconOne = document.querySelectorAll(".fas")[0];
let iconTwo = document.querySelectorAll(".fas")[1];

let message = document.querySelector(".message");

// change icon 1 -2
// icon 1
iconOne.addEventListener("click", function () {
  iconOne.classList.contains("fa-eye-slash")
    ? iconOne.classList.remove("fa-eye-slash")
    : iconOne.classList.add("fa-eye-slash");

  inputOne.type === "text"
    ? (inputOne.type = "password")
    : (inputOne.type = "text");
});
// icon 2
iconTwo.addEventListener("click", function () {
  iconTwo.classList.contains("fa-eye-slash")
    ? iconTwo.classList.remove("fa-eye-slash")
    : iconTwo.classList.add("fa-eye-slash");

  inputTwo.type === "text"
    ? (inputTwo.type = "password")
    : (inputTwo.type = "text");
});

// compare values of inputs
button.addEventListener("click", function () {
  let valueInputOne = inputOne.value;
  let valueInputTwo = inputTwo.value;
  if (valueInputOne === valueInputTwo) {
    message.textContent ? alert("You are welcome!") : alert("You are welcome!");
  } else {
    message.textContent = "Потрібно ввести однакові значення";
  }
});
